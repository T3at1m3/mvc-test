<?php

class PageController_images
{
	protected static $imgAvailableSize = array(
				"320x240",
				"640x480",
				"1024x768",
	);
	
	public static function index()
	{
		$filesDirectory = FileModel::getFilesFromDir('uploads');
		foreach($filesDirectory as $file)
		{
			list($width, $height) = getimagesize("uploads/".$file);
			$files[] = array(
				'fileName' 	=> $file,
				'width' 	=> $width,
				'height'	=> $height
				);
		}
		if(isset($_FILES['file_upload']))
		{
			$msg = FileModel::fileCheck($_FILES['file_upload']);
			echo View::Template('images', array(
										'files' => $files,
										'msg' 	=> $msg,
										));
		}
		elseif(isset($_POST['fileName']))
		{
			$msg = FileModel::removeFile($_POST['fileName']);
			echo View::Template('images', array(
									'files' => $files,
									'msg' 	=> $msg,
									));
		}
		else
		{
			echo View::Template('images', array('files' => $files));
		}
	}
	public static function checkImgSize($size)
	{
		$result = in_array($size, self::$imgAvailableSize);
		return $result;
	}

	public function resize()
	{
		$uri = Router::$URNParts;
		$uriExp = explode('x', $uri[2]);
		$result = self::checkImgSize($uri[2]);
		if($result)
		{
			$status = ImgResize::resizeImage($uri[3], $uriExp[0], $uriExp[1]);
			if($status)
			{
				self::show();
			}
			else
			{
				echo View::template('resize', array(
							'img' => $uri[3],
							'uri' => $uri,
							'msg' => $msg
							));
			}
		}
		else
		{
			$msg = 'File not found!';
			echo View::template('resize', array(
							'img' => $uri[3],
							'uri' => $uri,
							'msg' => $msg
							));
		}
	}
	public static function show()
	{
		$uri = Router::$URNParts;
		if(isset($_POST['dirName']))
		{
			$msg = FileModel::delDir($_POST['dirName']);
			if($msg)
			{
				$msg = 'Deleted!';
			}
			else
			{
				$msg = 'Nope!';
			}
		}
		if(!$uri)
		{
			$files = FileModel::getFilesFromDir('resimages/');
			echo View::template('images', array( 
								'files' => $files,
								'uri' => $uri,
								'msg' => $msg
									));
		}
		elseif($uri && !$uri[3])
		{
			$filesDirectory = FileModel::getFilesFromDir('resimages/'.$uri[2]);
			foreach ($filesDirectory as $file)
			{
				list($width, $height) = getimagesize("resimages/".$uri[2].'/'.$file);
				$files[] = array(
					'fileName' 	=> $file,
					'width' 	=> $width,
					'height'	=> $height
					);
			}
			echo View::template('images', array(
							'files' => $files,
							'uri' => $uri
								));
		}
		elseif(!empty($uri[3]))
		{
			if(!file_exists("resimages".DIRECTORY_SEPARATOR.$uri[2].DIRECTORY_SEPARATOR.$uri[3]))
			{
				self::resize();
			}
			else
			{
				echo View::template('nocssimage', array(
						'img' => $uri[2].'/'.$uri[3],
						'uri' => $uri,
						'files' => $files
						));
			}
		}
	}
	public static function showImage()
	{
		$uri = Router::$URNParts;
		if(!empty($uri[3]))
		{
			if(!file_exists("resimages".DIRECTORY_SEPARATOR.$uri[2].DIRECTORY_SEPARATOR.$uri[3]))
			{
				self::resize();
			}
			else 
			{	
				list($width, $height) = getimagesize("resimages/".$uri[2].'/'.$uri[3]);
				$files[] = array(
					'fileName' 	=> $uri[3],
					'width' 	=> $width,
					'height'	=> $height
					);
				echo View::template('theimage', array(
						'img' => $uri[2].'/'.$uri[3],
						'uri' => $uri,
						'files' => $files
						));
			}
		}
	}
}
