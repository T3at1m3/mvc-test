<?php

class PageController_rss
{
	public static function index()
	{
		$date = date("Y-m-d");
		$res = RSS::getMoviesBy('from_date',$date);
		echo View::template('rss', array('data' => $res));
	}

	public static function city()
	{
		$city = Router::$URNParts[2];
		if(!empty($city))
		{
			$city = stripslashes(urldecode($city));
			$res = RSS::getMoviesBy('city',$city);
			if(empty($res))
			{
				echo View::template('rss');
			}
			else
			{
				echo View::template('rss', array('data' => $res));
			}
		}
		else
		{
			echo View::template('rss');
		}
	}

	public static function movie()
	{
		$movie = Router::$URNParts[2];
		if(!empty($movie))
		{
			$movie = stripslashes(urldecode($movie));
			$res = RSS::getMoviesBy('movie', $movie);
			if(empty($res))
			{
				echo View::template('rss');
			}
			else
			{
				echo View::template('rss', array('data' => $res));
			}
		}
		else
		{
			echo View::template('rss');
		}
	}

	public static function cinema()
	{
		$cinema = Router::$URNParts[2];
		if(!empty($cinema))
		{
			$cinema = stripslashes(urldecode($cinema));
			$res = RSS::getMoviesBy('cinema' ,$cinema);
			if(empty($res))
			{
				echo View::template('rss');
			}
			else
			{
				echo View::template('rss', array('data' => $res));
			}
		}
		else
		{
			echo View::template('rss');
		}
	}

	public static function date()
	{
		$date = Router::$URNParts[2];
		if(!empty($date))
		{
			$date = stripslashes(urldecode($date));
			$res = RSS::getMoviesBy('from_date',$date);
			if(empty($res))
			{
				echo View::template('rss');
			}
			else
			{
				echo View::template('rss', array('data' => $res));
			}
		}
		else
		{
			echo View::template('rss');
		}
	}
}