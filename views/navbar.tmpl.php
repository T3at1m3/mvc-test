<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
    	<div class="navbar-header">
    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
      	</button>
      		<a class="navbar-brand" href="/">
        		<div class="image-brand"></div>
      		</a>
    	</div>
    	<div class="collapse navbar-collapse">
     		<ul class="nav navbar-nav">
     			<li><a href="/movies">Movies</a></li>
     			<li><a href="/rss">RSS</a></li>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Images <span class="caret"></span></a>
             <ul class="dropdown-menu" role="menu">
              <li><a href="/images">Upload Image</a></li>
              <li><a href="/images/show">Show Image</a></li>
             </ul>
          </li>
          <li><a href="/users">Users</a></li>
     		</ul>
     	</div>
  	</div>
</nav>