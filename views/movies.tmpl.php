<?php echo View::template('html' , array('title' => 'Movies')); ?>	
	<div class="container">
	<?php
	if($msg)
	{
		echo '<div class="alert alert-info text-center">'.$msg.'</div>';
	}
	?>
		<div class="well">
			<h2 class="text-center">Upload form:</h2>
			<form method="post" enctype="multipart/form-data" action="#">
			<div class="text-center">
				<input class="btn btn-primary" style="margin: 0 auto;" type="file" name="table_upload">
				<input class="btn btn-info" style="margin-top: 5px;" type="submit">
			</div>
			</form>
		</div>
		
		<form class="form-inline" method='POST' name="paginate" id="paginate" action="/movies/index">
			<div class="form-group">
				<?php
				echo View::template('pagination', array( 
											'data' => $pagination,
											));
				?>
			</div>
			<p></p>
			<div class="form-group">
				<?php
				echo View::template('paged', array( 'data' => $links));
				?>
			</div>
			<select class="form-control" id="selectPagination" onchange="" name="paginatedDrop">
			  <option>10</option>
			  <option>15</option>
			  <option>20</option>
			  <option selected>25</option>
			  <option>50</option>
			</select>
			<div class="form-group pull-right" style="margin-right: 5px;">
   			 <label for="inputNumber">Limit:</label>
    		 <input type="number" class="form-control" name="paginateInput" id="inputNumber" placeholder="Input limit ...">
 			 </div>
		</form>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>№</th>
					<th>Филм</th>
					<th>Цена(лв.)</th>
					<th>Кино</th>
					<th>Град</th>
					<th>Час:</th>
					<th>От (дата):</th>
					<th>До (дата):</th>
					<th>Бележки</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($data as $row)
			{
				echo "<tr>
						<td>".$row['program_id']."</td>
						<td>".$row['movie']."</td>
						<td>".$row['price']."</td>
						<td>".$row['cinema']."</td>
						<td>".$row['city_name']."</td>
						<td>".$row['time']."</td>
						<td>".$row['from_date']."</td>
						<td>".$row['till_date']."</td>
						<td>".$row['note']."</td>
					</tr>";
			}
			?> 
			</tbody>
		</table>
		<?php
				echo View::template('pagination', array( 
											'data' => $pagination,
											));
				?>
	</div>
	<script>
	$(document).ready(function() {
		$('#selectPagination').on('change', function(){
			$(this).find("option[selected='true']").removeAttr('selected');
        	$(this).find('option:selected').attr('selected', 'true');
        	var x = document.getElementById('selectPagination').value;
			document.paginate.submit(x);
		});
	});
	
	
	</script>