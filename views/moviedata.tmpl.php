<?php echo View::template('html', array('title' => 'Movie Data')); ?>
	<div class="container">
		<div class="page-header">
  			<h1>Movie data</h1>
		</div>
		<?php
		if(!empty($msg))
		{
			echo '<div class="alert alert-info">'.$msg.'</div>';
		}
		foreach($data as $row)
		{
			if($row['price'] == 0)
			{
				$price = 'Няма информация.';
			}
			else
			{
				$price = $row['price'].' лв.';
			}
			$html = '<div class="panel panel-primary"  id="movie-info">
			<div class="panel-heading">
					<h4 class="panel-titile">'.$row['movie'].'</h4></div>';
			$html .= '<div class="panel-body">
					<ul class="list-group">
						<li class="list-group-item">
							<span class="label label-primary">Кино:</span> ' . $row['cinema'].'</li>
						<li class="list-group-item">
							<span class="label label-primary">Град:</span> ' . $row['city_name'].'</li>
						<li class="list-group-item">
							<span class="label label-primary">Цена:</span> ' . $price.'</li>
						<li class="list-group-item">
							<span class="label label-primary">Час:</span> ' . $row['time']. '</li>
						<li class="list-group-item">
							<span class="label label-primary">От дата:</span> '.$row['from_date'].'</li>
						<li class="list-group-item">
							<span class="label label-primary">До дата:</span> '.$row['till_date'].'</li>
					</ul>
					</div>';
			$html .= '<div class="panel-footer">'.$row['note'].'</div></div>';
			echo $html;
		}
		?>
	</div>
</body>
</html>
