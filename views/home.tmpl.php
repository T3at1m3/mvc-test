 <?php  
 echo View::template('html' , array('title' => 'Homepage')); ?>
<div class="container">
<div class="jumbotron">
	<div class="container">
		<h1 class="text-center">Welcome!</h1>
		<p class="text-center">You can do the following: </p>
		<hr>
		<div class="col-md-4 text-center">
		<p>The program for Bulgarian theaters in all the major sities.</p>
			<a href="/movies" class="btn btn-primary">Theater Program</a>

		</div>
		<div class="col-md-4 text-center">
		<p>Place the theater program via RSS feed for your site.</p>
			<a href="/rss" class="btn btn-warning">RSS Feed</a>
		</div>
		<div class="col-md-4 text-center">
		<p>Upload an image. Resize it and see it in the gallery.</p>
			<a href="/images" class="btn btn-info">Image Upload</a>
		</div>
	</div>
</div>
</div>