<?php  
echo View::template('html' , array('title' => 'Homepage')); ?>
<div class="container">
	<div class="row">
 		<div class="half-div">
 			<ul class="nav nav-tabs">
 				<li><a href="#login" class="active" data-toggle="tab">Login</a></li>
 				<li><a href="#create" data-toggle="tab">Create Acc</a></li>
 			</ul>
 			<div id="myTabContent" class="tab-content">
             <div class="tab-pane active in" id="login">
              <form id="loginform" class="form-horizontal" role="form" method="POST">       
               <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email">
               </div>
               <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
               <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
               </div>
			   <div class="input-group">
                <div class="checkbox remember-me">
                <label>
                 <input id="login-remember" type="checkbox" name="remember" value="1">Remember me
                </label>
                </div>
               </div>
			   <div class="form-group">
               <!-- Button -->
                <div class="col-sm-12 controls">
                 <button type="submit" name="login" id="btn-login" class="btn btn-primary">Login</button>
                </div>
               </div>    
              </form>                
             </div>
             <div class="tab-pane fade" id="create">
              <form id="createform" role="form" method="POST" class="form-horizontal">
              <div class="input-group create-form">
              	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              	<input type="text" name="username" class="form-control" placeholder="Username">
              </div>
              <div class="input-group create-form">
              	<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              	<input type="email" name="email" class="form-control" placeholder="E-mail">
              </div>
              <div class="input-group create-form">
              	<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
              	<input type="text" name="firstname" class="form-control" placeholder="First Name">
              </div>
              <div class="input-group create-form">
              	<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
              	<input type="text" name="lastname" class="form-control" placeholder="Last Name">
              </div>
              <div class="input-group create-form">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
               <input type="password" class="form-control" name="password" placeholder="Input Password">
               </div>
               <div class="input-group create-form">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
               <input type="password" class="form-control" name="repassword" placeholder="Repeat Password">
               </div>
               <div class="input-group create-form">
              	<span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>
              	<input type="text" name="age" class="form-control" placeholder="Insert your age">
               </div>
               <div class="input-group create-form-bottom">
               <label>Choose City:</label>
               	<select class="form-control" name="cities" id="cities">
				<?php 
               	foreach($cities as $city) 
       			{ ?>
               		<option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
       			<?php }
               	?>
               	</select>
               </div>
               <div class="form-group">
               <!-- Button -->
                <div class="col-sm-12 controls">
                 <button id="btn-reg" type="submit" name="create" class="btn btn-primary">Register</button>
                </div>
               </div>
              </form>
             </div>
            </div>
 		</div>
 	</div>
</div>