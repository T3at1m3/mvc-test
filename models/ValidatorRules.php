<?php

class ValidatorRules
{
	private static $rules = array( 
						"regRules" => array(
							'username'  => 'minLenght:5|maxLenght:10|required',
							'email'		=> 'email|required',
							'firstname' => 'firstUpper|required',
							'lastname'	=> 'firstUpper|required',
							'password'	=> 'password|minLenght:8|required',
							'repassword'=> 'same:password',
							'age'		=> 'numeric|required'
									),
						'loginRules' => array(

							),
						);
	public static function getRules($ruleName)
	{	
		
		if(isset(self::$rules[$ruleName]))
		{
			return self::$rules[$ruleName];
		}
		else
		{
			return false;
		}
	}
}