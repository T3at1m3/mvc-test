<?php
class View
{
  
	public static function template($templateName, $data1 = array())
	{

		$file = 'views/'. $templateName . '.tmpl.php';

		extract($data1);
		unset ($data1);

		ob_start();
		include ($file);
		$out = ob_get_clean();

		return $out;
	}
}
