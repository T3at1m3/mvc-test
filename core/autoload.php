<?php

require_once ("core/Settings.php");

function __autoload($className)
{
		foreach(Settings::$autoloadPath as $path)
		{
			$file = $path."/".$className.".php";
			if (file_exists($file))
			{
				require_once($file);
				break;
			}
		}
}
